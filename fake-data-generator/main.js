const fs = require('fs');
const faker = require('faker');

const numUsers = 5;
const notesPerUser = 20;

const generateUser = () => ({
	id: faker.random.uuid(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  dateOfBirth: faker.date.past(),
});

const generateNote = (createById) => ({
  id: faker.random.uuid(),
  title: faker.name.title(),
  content: faker.lorem.paragraphs(),
  createById,
});

const main = () => {
  const users = [];
  const notes = [];
  for (let i = 0; i < numUsers; i += 1) {
    const currentUser = generateUser();
    for (let i = 0; i < notesPerUser; i += 1) {
      notes.push(generateNote(currentUser.id));
    }
    users.push(currentUser);
  }
  const usersJson = fs.createWriteStream('./users.json');
  const notesJson = fs.createWriteStream('./notes.json');
  usersJson.on('open', _ => usersJson.write(JSON.stringify(users)));
  notesJson.on('open', _ => notesJson.write(JSON.stringify(notes)));
}

main();

