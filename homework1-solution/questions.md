## Yêu cầu:
- Mỗi bạn tạo một project mới hòa toàn: từ git (git init) và npm (npm init)
- Thêm editorconfig, eslint airbnb
- Tạo thư mục `src` (ngang hàng mới `package.json`)
- Các bài tập được tạo bên trong thư mục src: ví dụ: `b1.js`, `b2.js`
- Code được push lên một repo mới trên gitlab (tạo mới rồi inbox đường dẫn cho anh nha, nhớ chọn public)

## Bài tập:
__1. Tìm hiểu sự khác biệt giữa `var`, `const` và `let` khi khai báo biết. Minh họa sự khác biệt đó bằng code.__

__2. Viết một hàm tính diện tích của hình tròn với bán kính cho trước:__
- input: r (number)
- output: diện tích (number)

__3. Viết hàm tìm học lực của một Lan với điểm số cho trước (từ 0 - 100). Với điều kiện:__
- 0 < score <= 25: grade = E
- 25 < score <= 40: grade = D
- 40 < score <= 65: grade = C
- 65 < score <= 80: grade = B
- 80 < score <= 100: grade = A

(Learn english !)

__4. Complete the getLetter(s) function in the editor. It has one parameter: a string `s`, consisting of lowercase English alphabetic letters (i.e., a through z). It must return A, B, C, or D depending on the following criteria:__

- If the first character in string `s`  is in the set `{a, e, i, o, u}`, then return A.
- If the first character in string `s` is in the set `{b, c, d, f, g}`, then return B.
- If the first character in string `s` is in the set `{h, j, k, l, m}`, then return C.
- If the first character in string `s` is in the set `{n, p, q, r, s, t, v, w, x, y, z}`, then return D.

Hint: You can get the letter at some index  in  using the syntax s[i] or s.charAt(i). (Using switch-case)

__5. Complete the `vowelsAndConsonants` function in the editor below. It has one parameter, a string `s` , consisting of lowercase English alphabetic letters (i.e., a through z). The function must do the following:__

- First, print each vowel in `s` on a new line. The English vowels are a, e, i, o, and u, and each vowel must be printed in the same order as it appeared in .
- Second, print each consonant (i.e., non-vowel) in `s`  on a new line in the same order as it appeared in `s`.

*Example:*
- Input: `javascriptloops`

- Output:
```
a
a
i
o
o
j
v
s
c
r
p
t
l
p
s
`
