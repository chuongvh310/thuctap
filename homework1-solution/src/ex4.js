/**
 * Try to guess the first character in a set of letters
 * @param {string} englishLetter alphabet english letters
 */
const getLetterOf = (englishLetter) => {
  // get the first letter
  let firstLetter = englishLetter ? englishLetter.charAt(0) : '';
  // make sure it's lowercase
  firstLetter = firstLetter.toLowerCase();
  const aGroups = ['a', 'e', 'i', 'o', 'u'];
  if (aGroups.indexOf(firstLetter) !== -1) {
    return 'A';
  }

  const bGroups = ['b', 'c', 'd', 'f', 'g'];
  if (bGroups.indexOf(firstLetter) !== -1) {
    return 'B';
  }

  const cGroups = ['h', 'j', 'k', 'l', 'm'];
  if (cGroups.indexOf(firstLetter) !== -1) {
    return 'C';
  }

  const dGroups = ['n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'];
  if (dGroups.indexOf(firstLetter) !== -1) {
    return 'D';
  }

  return 'LOL WTF letter';
};

console.log(getLetterOf('voila'));
