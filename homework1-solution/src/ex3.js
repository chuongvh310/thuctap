/**
 * Find the grade {A, B, C, D, E} based on score of a student
 * @param {number} score student score
 */
const findGradeOfStudent = (score) => {
  if (score <= 25) {
    return 'E';
  }
  if (score <= 40) {
    return 'D';
  }
  if (score <= 65) {
    return 'C';
  }
  if (score <= 80) {
    return 'B';
  }
  if (score <= 100) {
    return 'A';
  }
  return 'LOL WTF Score';
}; // remember `;`
console.log(findGradeOfStudent(80));

// challenge yourself with arrow function!
