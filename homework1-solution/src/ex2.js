/**
 * Calcuate the area of a cicle given radius
 * @param {number} radius the radius of the cicle
 * @returns area
 */
function calculateCirleArea(radius) {
  return radius * radius * Math.PI;
}

console.log(calculateCirleArea(10));

// Try to apply the arrow functions

/**
 * Calcuate the area of a cicle given radius
 * @param {number} radius the radius of the cicle
 * @returns area
 */
const calculateCirleAreaArrow = (radius) => {
  return radius * radius * Math.PI;
};

console.log(calculateCirleAreaArrow(10));

// it can be more simple
/**
 * Calcuate the area of a cicle given radius
 * @param {number} radius the radius of the cicle
 * @returns area
 */
const calculateCirleAreaArrowSimple = radius => radius * radius * Math.PI;

console.log(calculateCirleAreaArrowSimple(10));
