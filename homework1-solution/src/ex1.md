# Let, Const, Var cookbooks

**Rules**
- Preferer using `const`
- If you can't use `const`, use `let`
- NEVER USE `var`

|| `var` | `const` | `let` |
|------| ------|---------|-------|
| Block scope | `function` | `{}` | `{}` |
| Hoisting | Yes | No | No |
| Can assign another value | Yes | No | Yes |
