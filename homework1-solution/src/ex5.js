const vowels = ['a', 'e', 'i', 'o', 'u'];

/**
 * Check a character is vowel or not
 * @param {string} char a character
 */
const isVowel = char => vowels.indexOf(char) !== -1;

/**
 * Print vowels first and then non-vowels (simple method)
 * @param {string} englishLetter just random string
 */
const vowelsAndConsonants = (englishLetter) => {
  const normalized = (englishLetter || '').toLowerCase();
  let vowelsStr = '';
  let consonantsStr = '';
  for (let i = 0; i < normalized.length; i += 1) {
    const currentChar = normalized.charAt(i);
    if (isVowel(currentChar)) {
      vowelsStr += `${currentChar}\n`;
    } else {
      consonantsStr += `${currentChar}\n`;
    }
  }
  console.log(vowelsStr);
  console.log(consonantsStr);
};

console.log('by simple way');
vowelsAndConsonants('javascriptloops');

/**
 * Print vowels first and then non-vowels (a little advanced)
 * @param {string} englishLetter just random string
 */
const vowelsAndConsonantsAdvanced = (englishLetter) => {
  // if englishLetter is empty or null, use empty string
  const normalized = (englishLetter || '').toLowerCase();
  // convert to array of character
  const letters = normalized.split('');
  // try with filters
  const vowelsInLetter = letters.filter(character => isVowel(character));
  // or we can do this
  // const vowelsInLetter = letters.filter(isVowel);
  const consonants = letters.filter(character => !isVowel(character));
  // now join array to string with new line
  const vowelsString = vowelsInLetter.join('\n');
  const consonantsStr = consonants.join('\n');
  console.log(vowelsString);
  console.log(consonantsStr);
};

console.log('advanced');
vowelsAndConsonantsAdvanced('javascriptloops');
