const express = require('express');
const path = require('path');
const http = require('http');
const socketio = require('socket.io');

const app = express();

app.get('/', (req, resp) => {
  const rootPath = path.dirname(__dirname);
  const indexPath = path.join(rootPath, 'public', 'index.html');
  resp.sendFile(indexPath);
});

const server = http.createServer(app);

const io = socketio(server);

io.on('connection', (socket) => {
  console.log('a user connected');
  socket.on('disconnect', () => console.log('user disconnect'));

  socket.on('chat message', (message) => {
    socket.broadcast.emit('chat message', message);
  });
});

const port = +process.env.PORT || 4000;

server.listen(port, () => {
  console.log('listening on ' + port);
});
