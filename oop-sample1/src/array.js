const a = [4];

const b = [1, 2, 3];

// a.push(b);
a.push(...b);
console.log(a);

const user = {
  id: 1,
  username: 'a',
};


const notes = [{id: 1, content: 'cccc'}, {}];

user.notes = notes;

const userOutput = {
  id: 1,
  username: 'a',
  notes: [{id: 1, content: 'cccc'}, {}],
  // notes: ['ccc', 'aa'] // list of content
};


user.notes = notes;
user['notes'] = notes;

console.log(user);
// resp.json(user);
