const person = {
  fistName: 'Q',
  lastName: 'PH',
  yearOfBirth: 1994,
  // phương thức
  say() { // function say
    console.log(`Hello, I'm ${this.fistName}`);
  },
  say2: () => console.log(`Hello I'm ${person.fistName}`),

  age() {
    return 2018 - this.yearOfBirth;
  },
};

console.log('object');
person.say();
person.say2();

class Person { // CamelCase
  // Person() {}: C# and Java
  // không có overload
  constructor(fistName, lastName) {
    this.firstName = fistName;
    this.lastName = lastName;
    // this.say2 = () => console.log(`Hello I'm ${this.firstName} from class`);
  }

  say() {
    console.log(`Hello I'm ${this.firstName} from class`);
  }

  say2() {
    this.say();
  }
}

const q = new Person('q', 'ph');
q.say();
q.say2();


// Collection -> Table
// Document -> Row
class BaseCollection {
  findById(id) {
    return { id: 1 };
  }


  insertOne(document) {
    return 'success';
  }
}

class UserCollection extends BaseCollection {
  //
}

class LearningClassCollection extends BaseCollection {
  //
}

const userModel = new UserCollection();
console.log('insert ' + userModel.insertOne({name: 'q'}));

const learningModel = new LearningClassCollection();
console.log('find ' + learningModel.findById(1).id);
