const people = [
  { name: 'Wes', year: 1988 },
  { name: 'Kait', year: 1986 },
  { name: 'Irv', year: 1970 },
  { name: 'Lux', year: 2015 },
];
// calculate age in current year (2018)
const currentyear = new Date().getFullYear();
console.log('current year is', currentyear);
// ref: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
const peopleWithAge = people.map((person) => Object.assign({}, person, { age: currentyear - person.year }));

const isOne19 = peopleWithAge.some(person => person.age >= 19);
console.log('is at least one person 19 or older', isOne19);

const isEvert19 = peopleWithAge.every(person => person.age >= 19);
console.log('is everyone 19 or older ?', isEvert19);

const comments = [
  { text: 'Love this!', id: 523423 },
  { text: 'Super good', id: 823423 },
  { text: 'You are the best', id: 2039842 },
  { text: 'Ramen is my fav food ever', id: 123523 },
  { text: 'Nice Nice Nice!', id: 542328 }
];

const comment823423 = comments.find(comment => comment.id === 823423);
console.log('comment823423 is ', comment823423);

// to delete the comment with ID of 823423, find index in this arrat first
console.log('Comments after deleted comment with Id of 823423');
const comment823423Index = comments.findIndex(comment => comment.id === 823423);
if (comment823423Index !== -1) {
  // this is find, but never do this
  // because it modify original array
  // comments.splice(comment823423Index, 1);
  // console.log(comments);
  const newComments = [
    ...comments.slice(0, comment823423Index),
    ...comments.slice(comment823423Index + 1),
  ];
  console.log(newComments);
}

