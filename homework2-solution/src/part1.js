const inventors = [
    { first: 'Albert', last: 'Einstein', year: 1879, passed: 1955 },
    { first: 'Isaac', last: 'Newton', year: 1643, passed: 1727 },
    { first: 'Galileo', last: 'Galilei', year: 1564, passed: 1642 },
    { first: 'Marie', last: 'Curie', year: 1867, passed: 1934 },
    { first: 'Johannes', last: 'Kepler', year: 1571, passed: 1630 },
    { first: 'Nicolaus', last: 'Copernicus', year: 1473, passed: 1543 },
    { first: 'Max', last: 'Planck', year: 1858, passed: 1947 },
    { first: 'Katherine', last: 'Blodgett', year: 1898, passed: 1979 },
    { first: 'Ada', last: 'Lovelace', year: 1815, passed: 1852 },
    { first: 'Sarah E.', last: 'Goode', year: 1855, passed: 1905 },
    { first: 'Lise', last: 'Meitner', year: 1878, passed: 1968 },
    { first: 'Hanna', last: 'Hammarström', year: 1829, passed: 1909 },
];

console.log('1. Lọc ra danh sách nhà khoa học sáng học (inventors) sinh ra trong những năm 1500');
const inventors1500 = inventors.filter(inventor => inventor.year >= 1500 && inventor.year < 1600);
console.log(inventors1500);
// add new line
console.log('\n');

console.log('2. Đưa ra danh sách tên các nhà khoa học theo mẫu: <tên> <họ>. (all inventors)');
const inventorNames = inventors.map(inventor => `${inventor.first} ${inventor.last}`);
console.log(inventorNames);
console.log('\n');

console.log('3. Sắp xếp dánh sách của các nhà khoa học theo năm sinh (lớn trước, nhỏ sau)');
// reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort#Description
// copy danh sách inventors, (để không động gì đến danh sách ban đầu)
const yearSortInventors = [...inventors];
yearSortInventors.sort((inventor1, inventor2) => inventor2.year - inventor2.year);
console.log(yearSortInventors);
console.log('\n');

console.log('4. Các nhà khoa học có tổng tuổi là bao nhiêu ?');
const ages = inventors
  // get the age of each inventors
  .map(inventor => inventor.passed - inventor.year)
  // use reduce to calcuate sum
  .reduce((accumulate, current) => accumulate + current, 0);
console.log(ages);
console.log('\n');

console.log('5. Sắp xếp các nhà khoa học theo số năm sống được (từ cao đến thấp)');
let ageSortInventors = [...inventors];
ageSortInventors = ageSortInventors.map((inventor) => {
  inventor.age = inventor.passed - inventor.year;
  return inventor;
});

ageSortInventors.sort((inventor1, inventor2) => inventor2.age - inventor1.age);
console.log(ageSortInventors);
console.log('\n');

console.log('6. Tính số lần lặp lại các từ khóa trong mảng sau:');
const data = ['car', 'car', 'truck', 'truck', 'bike', 'walk', 'car', 'van', 'bike', 'walk', 'car', 'van', 'car', 'truck' ];
console.log(data.join(', '));
const freq = data.reduce((accumulate, current) => {
  // get count of current key, fallback to 0 if it doesn't exist
  const currentCount = accumulate[current] || 0;
  accumulate[current] = currentCount + 1;
  return accumulate;
}, {});
console.log(freq);
