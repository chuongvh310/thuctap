# NodeJS Standard
Standard starter kits for nodejs application using ES6

## Mongodb core

### Chạy mongo server trước
- **Linux** `$ mkdir mongodb_database && mongod --dbpath ./mongodb_database`
- **Windows**: `md C:\data\db`và `mongod`

Chú phải có dòng `NETWORK  [initandlisten] waiting for connections on port 27017`

### MongoShell && Robo3t
các lệnh quan trọng:
- `db.getCollection('users').find({})`.
- `db.getCollection('users').findOne({})`.
- `db.getCollection('users').insertOne({})`.
- `db.getCollection('users').insertMany({})`.
- `db.getCollection('users').updateOne({}, { $set: {} })`.
- `db.getCollection('users').updateMany({}, { $set: {} })`.
- `db.getCollection('users').deleteOne({})`.
- `db.getCollection('users').deleteMany({})`.



## Get Started
- `$ npm i`
- `$ npm run dev`

## Features
- Eslint
- Editorconfig
- Autoreload with nodemon
Trong file package.json, đoạn scripts, ta thêm như sau
```json
"scripts": {
  "start": "node src/main",
  "dev": "nodemon --exec 'node --inspect src/main.js'"
}
```
Khi đó để chạy app ở production `npm run start` (or `npm start`)

Lúc dev có autoreload: `npm run dev`

- Debug via VSCode alongs with nodemon
+ Bấm F5 là chọn NodeJS
+ Xóa config như và để lại như sau, sau đó chọn `Add Configuration ...`

![](./public/debugger.png)

+ Chọn NodeJS Attach => Save
