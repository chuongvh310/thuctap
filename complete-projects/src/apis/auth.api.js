const { Router } = require('express');

const loginFormController = (req, resp) => {
  resp.render('login', {
    title: 'Login',
  });
};

const registerFormController = (req, resp) => {
  resp.render('register', { register: 0 });
};

const createAuthRoutes = () => {
  const authRoutes = Router();
  authRoutes.get('/login', loginFormController);
  authRoutes.get('/register', registerFormController);
  return authRoutes;
};

const authRoutes = createAuthRoutes();

module.exports = authRoutes;
