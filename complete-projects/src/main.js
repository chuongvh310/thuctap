const path = require('path');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const utils = require('./utils');

const authRoutes = require('./apis/auth.api');

const main = () => {
  const app = express();
  app.use(bodyParser.json());
  app.use('/public', express.static(path.join(utils.rootPath, 'public')));
  app.set('view engine', 'ejs');
  // mặc định thì ejs sẽ tìm trong thư mự views
  // app.set('views', path.join(utils.rootPath, 'template'));

  // setting routes
  app.use('/', authRoutes);

  const server = http.createServer(app);
  const port = +process.env.PORT || 8080;
  server.listen(port, '0.0.0.0', (err) => {
    if (err) {
      console.log('Create server got an error', err);
    } else {
      console.log('Server listening', port);
    }
  });
};

main();
