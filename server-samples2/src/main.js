
const path  = require('path');
const express = require('express');
const apis = require('./api');

const utils = require('./utils');

const app = express();
app.set("view options", { layout: false });
// server chay file css, js
// middleware
app.use('/public', express.static(path.join(utils.rootPath, 'public')));

app.get('/', apis.homepageController);
app.get('/login', apis.loginController);
app.get('/register', apis.registerController);

const port = +process.env.PORT || 6060;

app.listen(port, (error) => {
  if (error) {
    return console.error('run server got an error', error);
  }
  return console.log(`Server listening ${port}`);
});
