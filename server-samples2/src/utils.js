const path = require('path');
const fs = require('fs');

// __dirname là biến đặc biệt của nodejs, là thư mục chứa file hiện tại
const srcDir = __dirname;
// root dir là thư mục chứa package.json
const root = path.dirname(srcDir);

/**
 * Đọc file và return Promise
 * @param {string} path
 * @return Promise
 */
const readFile = (path) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path, (err, data) => {
      return err ? reject(err) : resolve(data);
    });
  });
};

module.exports = {
  readFile,
  rootPath: root,
};