// xử lý url /
const helloController = (req, resp) => {
  resp.send('Hello world');
};

// xử lý url /hacker
const hackerController = (req, resp) => {
  resp.json({ hello: 'world' });
};

module.exports = {
  helloController,
  hackerController,
};
