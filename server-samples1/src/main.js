// sử dụng module express từ node_modules (third party)
const express = require('express');

// sử dụng module do mình viết (nhớ dấu ./)
const apis = require('./api');

// khai báo ứng dụng expressjs
const app = express();

// config cặp url - controller
app.get('/', apis.helloController);
app.get('/hacker', apis.hackerController);

// config port của ứng dụng
// lấy port từ biến môi trường PORT=11111 node src/main.js
const port = +process.env.PORT || 9999;

app.listen(port, (error) => {
  if (error) {
    return console.error('run server got an error', error);
  }
  console.log(`Server listening ${port}`);
});
