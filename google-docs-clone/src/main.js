const path = require('path');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const flash = require('connect-flash');
// 1. import sesssion & passport
const session = require('express-session');
const passport = require('passport');

const mongo = require('./config/database');

const ejs = require('ejs');
const utils = require('./utils');

const authRoutes = require('./apis/auth.api');

// 3. require config
require('./passport');

const main = async () => {
  await mongo.connect();
  const app = express();
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  // 2. config passport
  app.use(session({
    secret: 'aiashaksdjfh',
    resave: false,
    saveUninitialized: true,
    // h * min * s * 1000
    cookie: { maxAge: (7 * 24 * 60 * 60 * 1000) },
  }));
  app.use(passport.initialize());
  app.use(passport.session());
  // done passport

  app.use(flash());

  app.use('/public', express.static(path.join(utils.rootPath, 'public')));
  // config html extention that uses ejs engine
  app.set('view engine', 'html');
  app.engine('html', ejs.renderFile);
  // mặc định thì ejs sẽ tìm trong thư mự views

  // setting routes
  app.use('/', authRoutes);
  app.use('/', (req, resp) => {
    resp.render('home', { user: req.user });
  });

  const server = http.createServer(app);
  const port = +process.env.PORT || 8080;
  server.listen(port, '0.0.0.0', (err) => {
    if (err) {
      console.log('Create server got an error', err);
    } else {
      console.log('Server listening', port);
    }
  });
};

main();
