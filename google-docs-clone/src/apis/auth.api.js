const { Router } = require('express');
const passport = require('passport');

const loginFormController = (req, resp) => {
  const error = req.flash('error');
  const infoMessage = req.flash('info');
  resp.render('login', {
    error,
    title: 'Login',
    message: infoMessage,
  });
};

const loginController = (req, resp) => {
  resp.redirect('/');
};

const registerFormController = (req, resp) => {
  const payload = {
    title: 'Register',
    error: undefined,
  };
  const errors = req.flash('register_error');
  if (errors && errors.length !== 0) {
    payload.error = errors[0];
  }
  return resp.render('register', payload);
};

const registerController = async (req, resp) => {
  // form has error
  const error = {
    username: 'invalid username',
  };
  req.flash('register_error', error);
  return resp.redirect('/register');
  // register successful, go to login page
  // return resp.redirect('/login');
};

const createAuthRoutes = () => {
  const authRoutes = Router();
  authRoutes.get('/login', loginFormController);
  authRoutes.post('/login', passport.authenticate('local', { failureRedirect: '/login' }), loginController);

  authRoutes.get('/register', registerFormController);
  authRoutes.post('/register', registerController);
  return authRoutes;
};

const authRoutes = createAuthRoutes();

module.exports = authRoutes;
