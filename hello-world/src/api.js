const homeController = (req, res) => {
  // res.sendFile('index.html');
  res.send('Hello World');
};

const hackerController = (req, res) => {
  res.json({ hello: 'world' });
};

module.exports = {
  homeController,
  hackerController,
};
