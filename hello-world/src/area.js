/**
 * Calcuate the area of a cicle given radius
 * @param {number} radius the radius of the cicle
 * @returns area
 */
const calculateCirleArea = radius => radius * radius * Math.PI;

const index = 1;
// module.exports.calculateCirleArea = calculateCirleArea;

module.exports = {
  // module khác có thể sử dụng được function này
  calculateCirleArea,
  index,
};
