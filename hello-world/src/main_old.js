const http = require('http');
const fs = require('fs');
const url = require('url');

const handler = (req, resp) => {
  const path = url.parse(req.url, true);
  // handle / path
  switch (path.pathname) {
    case '/': {
      resp.writeHead(200, { 'Content-Type': 'text/html' });
      fs.readFile('index.html', (err, data) => {
        resp.end(data);
      });
      return;
    }

    case '/hacker': {
      resp.writeHead(200, { 'Content-Type': 'application/json' });
      resp.end(JSON.stringify({ hello: 'world' }));
      return;
    }

    default:
      resp.writeHead(404);
      resp.end();
      break;
  }
};

const server = http.createServer(handler);

const port = +process.env.PORT || 9999;

server.listen(port, (err) => {
  if (err) {
    return console.log(err);
  }
  return console.log(`server listening ${port}`);
});
