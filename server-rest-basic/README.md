# NodeJS Standard
Standard starter kits for nodejs application using ES6

## Get Started
- `$ npm i`
- `$ npm run dev`

## Features
- Eslint
- Editorconfig
- Autoreload with nodemon
Trong file package.json, đoạn scripts, ta thêm như sau
```json
"scripts": {
  "start": "node src/main",
  "dev": "nodemon --exec 'node --inspect src/main.js'"
}
```
Khi đó để chạy app ở production `npm run start` (or `npm start`)

Lúc dev có autoreload: `npm run dev`

- Debug via VSCode alongs with nodemon
+ Bấm F5 là chọn NodeJS
+ Xóa config như và để lại như sau, sau đó chọn `Add Configuration ...`

![](./public/debugger.png)

+ Chọn NodeJS Attach => Save
