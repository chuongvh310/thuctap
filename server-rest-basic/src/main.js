
const path  = require('path');
const express = require('express');
const bodyParser = require('body-parser');

const utils = require('./utils');
const homeApis = require('./apis/home.api');
const authApis = require('./apis/auth.api');
const userApis = require('./apis/user.api');

const app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())
 
app.set("view options", { layout: false });
// server chay file css, js
// middleware
app.use('/public', express.static(path.join(utils.rootPath, 'public')));

app.get('/', homeApis.homepageController);
app.get('/login', authApis.loginController);
app.get('/register', authApis.registerController);
app.get('/users', userApis.findAllUsersController);
app.post('/users', userApis.createUserController);
app.put('/users/:id', userApis.updateuserController);
app.delete('/users/:id', userApis.deleteUserController);
app.get('/users/:id', userApis.findDetailUserController);

const port = +process.env.PORT || 6060;

app.listen(port, (error) => {
  if (error) {
    return console.error('run server got an error', error);
  }
  return console.log(`Server listening ${port}`);
});
