const path = require('path');

const utils = require('../utils');

class UserRepo {
  constructor() {}

  async allUsers() {
    const userJsonpath = path.join(utils.rootPath, 'data', 'users.json');
    const userContent = await utils.readFile(userJsonpath);
    const userJson = JSON.parse(userContent);
    return userJson;
  }

  async findById(userId) {
    const users = await this.allUsers();
    const userDetail = users.find(user => user.id === userId);
    return userDetail;
  }
}

const userRepo = new UserRepo();

// default exports
module.exports = userRepo;