const userRepo = require('../repository/users.repo');

const findAllUsersController = async (req, resp) => {
  const users = await userRepo.allUsers();
  resp.json(users);
};

const createUserController = (req, resp) => {
  const userForm = req.body;
  userForm.id = 'aadsjkfhckjahsdf';
  resp.json(userForm);
};

const updateuserController = (req, resp) => {
  const userId = req.params.id;
  console.log(userId);
  console.log(req.body);
  // update user info
  resp.json({ 'update': 'ok' });
};

/**
 * 
 * @param {Express.Request} req 
 * @param {Express.Response} resp 
 */
const deleteUserController = (req, resp) => {
  const userId = req.params.id;
  console.log('delete userId', userId);
  resp.sendStatus(204);
};

const findDetailUserController = async (req, resp) => {
  const userDetail = await userRepo.findById(req.params.id);
  // find notes
  // userDetail.notes = notes;
  resp.json(userDetail);
};

module.exports = {
  createUserController,
  findAllUsersController,
  updateuserController,
  deleteUserController,
  findDetailUserController,
};