const express = require('express');
const userRepo = require('../repository/users.mongo-repo');
const userService = require('../services/users.service');

const findAllUsersController = async (req, resp) => {
  const users = await userRepo.allUsers();
  resp.json(users);
};

const createUserController = async (req, resp) => {
  try {
    const userForm = req.body;
    const userInserted = await userService.createUser(userForm);
    resp.json(userInserted);
  } catch (err) {
    resp.status(400).json({ error: err });
  }
};

const updateuserController = async (req, resp) => {
  const userId = req.params.id;
  const isUpdated = await userRepo.updateById(userId, req.body);
  resp.json({ 'update': 'ok' });
};

/**
 *
 * @param {Express.Request} req 
 * @param {Express.Response} resp 
 */
const deleteUserController = async (req, resp) => {
  const userId = req.params.id;
  const isDeleted = await userRepo.deleteById(userId);
  console.log('delete userId', userId, isDeleted);
  resp.sendStatus(204);
};

const findDetailUserController = async (req, resp) => {
  const userDetail = await userRepo.findById(req.params.id);
  // find notes
  // userDetail.notes = notes;
  resp.json(userDetail);
};

const userRoutes = express.Router();

userRoutes.get('/', findAllUsersController);
userRoutes.post('/', createUserController);
userRoutes.put('/:id', updateuserController);
userRoutes.delete('/:id', deleteUserController);
userRoutes.get('/:id', findDetailUserController);

module.exports = {
  userRoutes,
};
