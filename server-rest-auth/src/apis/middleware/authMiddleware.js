const jwt = require('jsonwebtoken');

const jwtVerify = (token) => new Promise((resolve, reject) => {
  jwt.verify(token, 'secret', (err, decoded) => {
    return err ? reject(err) : resolve(decoded);
  });
});

const authMiddleware = async (req, resp, next) => {
  // example: Bearer thisispassword
  const authvalue = req.headers.authorization;
  if (!authvalue) {
    return resp.status(403).json({ error: 'invalid token' });
  }

  const tokens = authvalue.split(' ');
  if (tokens.length !== 2 || tokens[0] !== 'Bearer') {
    return resp.status(403).json({ error: 'invalid token' });
  }
  try {
    const payload = await jwtVerify(tokens[1]);
    console.log(payload);
    return next();
  } catch (error) {
    return resp.status(403).json({ error: 'invalid token' });
  }
};

module.exports = authMiddleware;
