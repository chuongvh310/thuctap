const { Router } = require('express');


const userService = require('../../services/users.service');

const adminCreateUserController = async (req, resp) => {
  const userForm = req.body;
  try {
    const newuser = await userService.createUser(userForm);
    resp.status(201).json(newuser);
  } catch (error) {
    resp.status(400).json({ error: error });
  }

};

const adminUserRoute = Router();

adminUserRoute.post('/', adminCreateUserController);

module.exports = adminUserRoute;

